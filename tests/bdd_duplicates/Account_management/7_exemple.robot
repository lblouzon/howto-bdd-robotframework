# Automation priority: 100
# Test case importance: Low
*** Settings ***
Resource	squash_resources.resource

*** Keywords ***
Test Setup
	${__TEST_SETUP}	Get Variable Value	${TEST SETUP}
	${__TEST_7_SETUP}	Get Variable Value	${TEST 7 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	${__TEST_SETUP}
	Run Keyword If	$__TEST_7_SETUP is not None	${__TEST_7_SETUP}

Test Teardown
	${__TEST_7_TEARDOWN}	Get Variable Value	${TEST 7 TEARDOWN}
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	Run Keyword If	$__TEST_7_TEARDOWN is not None	${__TEST_7_TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}

*** Test Cases ***
exemple
	[Setup]	Test Setup

	Given I am on the Login page
	When I can successfully sign in with email "olivier.mail.com" password "mon_password"
	Then The displayed name is "olivier"

	[Teardown]	Test Teardown