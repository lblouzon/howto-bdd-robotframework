# Automation priority: 42
# Test case importance: High
*** Settings ***
Resource	squash_resources.resource
Library		squash_tf.TFParamService

*** Keywords ***
Test Setup
	${__TEST_SETUP}	Get Variable Value	${TEST SETUP}
	${__TEST_244_SETUP}	Get Variable Value	${TEST 244 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	${__TEST_SETUP}
	Run Keyword If	$__TEST_244_SETUP is not None	${__TEST_244_SETUP}

Test Teardown
	${__TEST_244_TEARDOWN}	Get Variable Value	${TEST 244 TEARDOWN}
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	Run Keyword If	$__TEST_244_TEARDOWN is not None	${__TEST_244_TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}

*** Test Cases ***
Standard account creation
	${gender} =	Get Test Param	DS_gender
	${first} =	Get Test Param	DS_first
	${last} =	Get Test Param	DS_last
	${password} =	Get Test Param	DS_password
	${mail} =	Get Test Param	DS_mail
	${birth} =	Get Test Param	DS_birth
	${offers} =	Get Test Param	DS_offers
	${privacy} =	Get Test Param	DS_privacy
	${news} =	Get Test Param	DS_news
	${gpdr} =	Get Test Param	DS_gpdr

	[Setup]	Test Setup

	Given I am on the AccountCreation page
	When I fill AccountCreation fields with gender ${gender} firstName ${first} lastName ${last} password ${password} email ${mail} birthDate ${birth} acceptPartnerOffers ${offers} acceptPrivacyPolicy ${privacy} acceptNewsletter ${news} acceptGpdr ${gpdr} and submit
	And I sign out
	And I sign in with email ${mail} and password ${password}
	Then My personal informations are gender ${gender} firstName ${first} lastName ${last} email ${mail} birthDate ${birth}

	[Teardown]	Test Teardown