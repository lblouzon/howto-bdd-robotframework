# Automation priority: null
# Test case importance: High
*** Settings ***
Resource	squash_resources.resource

*** Keywords ***
Test Setup
	${__TEST_SETUP}	Get Variable Value	${TEST SETUP}
	${__TEST_248_SETUP}	Get Variable Value	${TEST 248 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	${__TEST_SETUP}
	Run Keyword If	$__TEST_248_SETUP is not None	${__TEST_248_SETUP}

Test Teardown
	${__TEST_248_TEARDOWN}	Get Variable Value	${TEST 248 TEARDOWN}
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	Run Keyword If	$__TEST_248_TEARDOWN is not None	${__TEST_248_TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}

*** Test Cases ***
KOtests_001
	[Setup]	Test Setup

	Given I am on the AccountCreation page
	When I fill AccountCreation fields with gender "F" firstName "Alice" lastName "Noel" password "police" email "alice@noel.com" birthDate "01/01/1970" acceptPartnerOffers "yes" acceptPrivacyPolicy "yes" acceptNewsletter "yes" acceptGpdr "yes" and submit
	And I sign out
	And I sign in with email "alice@noel.com" and password "police"
	Then My personal informations are gender "F" firstName "Bob" lastName "Alice" email "noel@alice.com" birthDate "02/02/1981"

	[Teardown]	Test Teardown