# Automation priority: null
# Test case importance: Low
*** Settings ***
Resource	squash_resources.resource

*** Keywords ***
Test Setup
	${__TEST_SETUP}	Get Variable Value	${TEST SETUP}
	${__TEST_249_SETUP}	Get Variable Value	${TEST 249 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	${__TEST_SETUP}
	Run Keyword If	$__TEST_249_SETUP is not None	${__TEST_249_SETUP}

Test Teardown
	${__TEST_249_TEARDOWN}	Get Variable Value	${TEST 249 TEARDOWN}
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	Run Keyword If	$__TEST_249_TEARDOWN is not None	${__TEST_249_TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}

*** Test Cases ***
Navigate to one product
	${docstring_1} =	Set Variable	Le meilleur reste à venir ! Faites parler vos murs avec cette affiche encadrée chargée d'optimisme sera du plus bel effet dans un bureau ou un open-space. Cadre en bois peint avec passe-partout integré pour un effet de profondeur.

	[Setup]	Test Setup

	When I navigate to category "art"
	And I navigate to product "Affiche encadrée The best is yet to come"
	Then The product description is "${docstring_1}"

	[Teardown]	Test Teardown